import React from 'react';
import Select from 'react-select';
import { Switch } from './controls';
import moment from 'moment'
import Calendar from 'react-calendar';

export const FlightSelector = ({ switchClicked, dropdownFromChanged, dropdownToChanged, valueFrom, valueTo, options }) => {
  const dropdownStyles = {
    control: styles => ({
      ...styles, height: "4vh", minHeight: "35px", minWidth: "30vw", width: "100%",
      backgroundColor: 'white', border: "0px", boxShadow: "0px",
      borderBottomLeftRadius: "20px", borderBottomRightRadius: "20px",
      borderTopLeftRadius: "0px", borderTopRightRadius: "0px",
      outline: "none"
    }),
    dropdownIndicator: styles => ({ ...styles, display: "none" }),
    valueContainer: styles => ({
      ...styles, minWidth: "30vw", height: "4vh", minHeight: "35px", width: "100%",
      backgroundColor: 'white',
      borderBottomLeftRadius: "20px", borderBottomRightRadius: "20px",
      borderTopLeftRadius: "0px", borderTopRightRadius: "0px",
      outline: "none"
    }),
    container: styles => ({ ...styles, minWidth: "30vw", width: "100%", height: "4vh", minHeight: "35px", outline: "none", flexGrow: 1 }),
    indicatorSeparator: styles => ({ ...styles, display: "none" }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        color: "black",
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? "#7DBFD8"
            : isFocused
              ? "#CCE9F4"
              : "white",
        ':active': {
          ...styles[':active'],
          backgroundColor: !isDisabled && ("#74B2CA"),
        },
      };
    },
    input: styles => ({ ...styles, outline: "none" }),
    menu: styles => ({ ...styles, borderRadius: "10px", border: "0px", color: "black" }),
    menuList: styles => ({ ...styles, borderRadius: "10px", border: "0px", padding: "0px", }),
    menuPortal: styles => ({ ...styles, border: "0px", }),
    placeholder: styles => ({ ...styles, backgroundColor: 'white', color: "gray" }),
    singleValue: (styles, { data }) => ({ ...styles, backgroundColor: 'white' }),
  };

  return (
    <div id="selectorContainer">
      <div className="selectorLabelContainer">
        <div className="selectorLabel">Odkud</div>
        <Select
          isSearchable="false"
          placeholder="Odkud"
          styles={dropdownStyles}
          value={valueFrom}
          options={options}
          onChange={dropdownFromChanged}
        />
      </div>
      <div id="switch-container">
        <Switch switchClicked={switchClicked} switchClassName={valueTo ? "visible-animate" : "hidden"} />
      </div>
      <div className="selectorLabelContainer">
        <div className="selectorLabel">Kam</div>
        <Select
          isSearchable="false"
          value={valueTo}
          styles={dropdownStyles}
          options={options}
          onChange={dropdownToChanged}
        />
      </div>
    </div>);
}

export const FlightCalendar = ({ monthClicked, date }) => (
  <div id="calendarContainer">
    {date ? "" : <label className="label-calendar">select month and year</label>}
    <Calendar
      value={date}
      minDate={new Date()}
      onClickMonth={monthClicked}
      className="calendar"
      tileClassName="calendar-tile"
      maxDetail="year"
    />
  </div>
);

export const FlightList = ({ flights }) => {
  return (
    <div id="flightList">
      {
        (Array.isArray(flights) && flights.length > 0) ?
          <table className="flightTable">
            <thead>
              <tr>
                <th>Date</th>
                <th>Price</th>
                <th>Seats</th>
                <th>Duration</th>
              </tr>
            </thead>
            <tbody>
              {flights.map(f => {
                return f.flights.map(ff => {
                  const key = ff.departureDateTime + ff.aircraftRef + ff.flightNumber + ff.duration;
                  return <tr key={key}>
                    <td>{moment(f.date).format("D.M.YYYY")}</td>
                    <td>{parseInt(f.price).toLocaleString("cs") + " CZK"}</td>
                    <td>{ff.seats}</td>
                    <td>{moment.utc().startOf('day').add({ minutes: +ff.duration }).format('H[  h] mm[ min]')}</td>
                  </tr>

                })



              })}
            </tbody>
          </table> : null
      }
    </div>
  );
}