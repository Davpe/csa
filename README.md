This is a frontend application for flight filtering using Czech Airlines API.
It uses<br />
[Create React App](https://github.com/facebook/create-react-app) for bootstrapping<br />
[React-Select](https://react-select.com/) for styled dropdown menu<br />
[React-Calendar](https://github.com/wojtekmaj/react-calendar/) for styled calendar<br />

To start the application in project directory run:<br/>

### `yarn install`
### `yarn start`

Which runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.