// 
export const defaultDestination = "Prague, Ruzyne (PRG)";
export const destUrl = "https://www.csa.cz/Umbraco/Api/DestinationCache/GetAllDestinations/?destinations_language=en";
export const getPricesUrl = (depCode, arrCode, parsedDate) => `https://www.csa.cz/Umbraco/Api/CalendarPricesCache/GetPrices/?DEP=${depCode}&ARR=${arrCode}&MONTH_SEL=${parsedDate}&SECTOR_ID=0&LANG=cs&ID_LOCATION=cz`;