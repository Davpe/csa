import React, { Component } from 'react';
import 'react-calendar/dist/Calendar.css'
import './App.css';
import { destUrl, getPricesUrl, defaultDestination } from './common';
import { Spinner, Error } from './controls';
import { FlightSelector, FlightCalendar, FlightList } from './flightControls';
import moment from 'moment'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      valueTo: null,
      valueFrom: null,
      options: [],
      flights: [],
      showSpinner: false,
      error: null,
    };

    this.DropdownToChanged = this.DropdownToChanged.bind(this);
    this.DropdownFromChanged = this.DropdownFromChanged.bind(this);
    this.SwitchClicked = this.SwitchClicked.bind(this);
    this.MonthClicked = this.MonthClicked.bind(this);
    this.FetchDestinations = this.FetchDestinations.bind(this);
    this.ShowError = this.ShowError.bind(this);
  }

  componentDidMount() {
    this.FetchDestinations();
  }

  RefreshFlights = (from, to, date) => {
    this.setState({ showSpinner: true });

    const depCode = from.value.code;
    const arrCode = to.value.code;
    const parsedDate = moment(date).format('MM/YYYY');

    fetch(getPricesUrl(depCode, arrCode, parsedDate), {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          const dayList = result?.calendarPriceList?.dayList;

          if (!Array.isArray(dayList))
            throw new Error("Daylist is not an array.");

          const availableFlights = dayList.filter(x => x.status === "AVAILABLE");
          this.setState({
            flights: availableFlights
          })

        }).catch(() => {
          this.ShowError("No flights found.")
        }).finally(() =>
          this.setState({ showSpinner: false })
        );
  }

  FetchDestinations = () => {
    fetch(destUrl, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          if (!Array.isArray(result))
            throw new Error("Flights are not an array.");

          const mapped = result.map(x => {
            return {
              value: { city: x.AirportCityName, code: x.AirportCode, name: x.AirportName },
              label: `${x.AirportCityName}, ${x.AirportName} (${x.AirportCode})`
            }
          });

          const defaultValue = mapped.find(opt => opt.label === defaultDestination);

          this.setState({
            options: mapped,
            valueFrom: defaultValue
          })
        }).catch(() => {
          this.ShowError("No destinations found")
        });
  }

  DropdownToChanged = (option) => {
    if (this.state.valueTo === option)
      return;

    this.setState({ valueTo: option })

    if (this.state.date) {
      this.RefreshFlights(this.state.valueFrom, option, this.state.date);
    }
  }

  DropdownFromChanged = (option) => {
    if (this.state.valueFrom === option)
      return;

    this.setState({ valueFrom: option })

    if (this.state.valueTo && this.state.date) {
      this.RefreshFlights(option, this.state.valueTo, this.state.date);
    }
  }

  SwitchClicked = () => {
    if (this.state.valueFrom === this.state.valueTo)
      return;

    const newFrom = this.state.valueTo;
    const newTo = this.state.valueFrom;

    this.setState({
      valueFrom: newFrom,
      valueTo: newTo
    })

    if (this.state.date) {
      this.RefreshFlights(newFrom, newTo, this.state.date);
    }
  }

  MonthClicked = (date) => {
    this.setState({ date: date });
    this.RefreshFlights(this.state.valueFrom, this.state.valueTo, date);
  }

  ShowError = (text) => {
    this.setState({ error: text }, () => {
      setTimeout(() => this.setState({ error: null }), 3000);
    })
  }

  render() {
    return (
      <div id="topContainer">
        <FlightSelector
          switchClicked={this.SwitchClicked}
          dropdownFromChanged={this.DropdownFromChanged}
          dropdownToChanged={this.DropdownToChanged}
          valueFrom={this.state.valueFrom}
          valueTo={this.state.valueTo}
          options={this.state.options}
        />
        <Error text={this.state.error} />
        <div id="flightsContainer" className={this.state.valueTo ? "visible-animate" : "hidden"}>
          <FlightCalendar monthClicked={this.MonthClicked} date={this.state.date} />
          <FlightList flights={this.state.flights} />
        </div>
        <Spinner showSpinner={this.state.showSpinner} />
      </div>
    );
  };
}

export default App;